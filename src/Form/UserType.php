<?php

namespace App\Form;

use App\Entity\Gender;
use App\Entity\Interest;
use App\Entity\Relation;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\NotNull;




class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('age')
            ->add('photoFile', FileType::class, [
                //on précise au formulaire de ne pas ranger lui même la donnée, car on ne veut stocker que le nom du fichier 
                'mapped' => false,
                'required' => true,
                //le champ n'étant pas mappé à l'entité on doit définir les contraintes ici
                'constraints' => [
                    new NotNull(),
                    //la contrainte de type File permet de définir des contraintes associées aux fichiers
                    new File([
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/jpg'
                        ],
                        'mimeTypesMessage' => 'Only PNG and JPG are allowed',
                        'maxSize' => '2048k'
                    ])
                ],
            ])
            ->add('interests', EntityType::class, [
                'class' => Interest::class,
                'choice_label' => 'name',
                'multiple' => 'true',
                'expanded' => 'true',
            ])
            ->add('relations', EntityType::class, [
                'class' => Relation::class,
                'choice_label' => 'name',
                'multiple' => 'false',
            ])
            ->add('genders', EntityType::class, [
                'class' => Gender::class,
                'choice_label' => 'name',
            ])
            ->add('description', TextareaType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
